//
//  ViewController.swift
//  AntBrowser
//
//  Created by Kyle Olsen on 2/18/16.
//  Copyright © 2016 Kyle Olsen. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    @IBOutlet var addressField: UITextField!
    @IBOutlet var backButton: UIBarButtonItem!
    @IBOutlet var reloadButton: UIBarButtonItem!
    @IBOutlet var forwardButton: UIBarButtonItem!
    @IBOutlet var requestActivityIndicator: UIActivityIndicatorView!
    var initToken: dispatch_once_t = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressField.delegate = self
        webView.delegate = self
        webView.scalesPageToFit = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        addressField.resignFirstResponder()
        loadPage(textField.text)
        return true
    }
    
    @IBAction func browseActionClicked(sender: UIBarButtonItem) {
        switch sender.tag {
        case -1:
            if webView.canGoBack {
                webView.goBack()
            }
        case 0:
            webView.reload()
        case 1:
            if webView.canGoForward {
                webView.goForward()
            }
        default:
            return
        }
        updateActionButtonStates()
    }
    
    @IBAction func searchClicked(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Search", message: "Enter a search term", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.text = ""
        })
        
        alert.addAction(UIAlertAction(title: "Go", style: .Default, handler: { (action) -> Void in
            let searchField = alert.textFields![0] as UITextField
            self.addressField.text = "https://www.google.com/?gws_rd=ssl#q=\(searchField.text!)"
            self.loadPage("https://www.google.com/?gws_rd=ssl#q=\(searchField.text!)")
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func loadPage(address: String?) {
        if var addr = address?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) {
            if !addr.hasPrefix("http://") && !addr.hasPrefix("https://") {
                addr = "http://" + addr
            }
            if let url = NSURL(string: addr) {
                webView.loadRequest(NSURLRequest(URL: url))

                addressField.text = url.absoluteString
                updateActionButtonStates()
                
                dispatch_once(&initToken) {
                    self.reloadButton.enabled = true
                }
            }
        }
    }
    
    func updateActionButtonStates() {
        if webView.canGoBack {
            backButton.enabled = true
        } else {
            backButton.enabled = false
        }
        if webView.canGoForward {
            forwardButton.enabled = true
        } else {
            forwardButton.enabled = false
        }
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        requestActivityIndicator.startAnimating()
        reloadButton.enabled = false
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        requestActivityIndicator.stopAnimating()
        reloadButton.enabled = true
        updateActionButtonStates()
    }
}

